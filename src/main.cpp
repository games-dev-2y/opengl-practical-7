#include <Game.h>

int main(void)
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 32u;

	Game& game = Game(settings);
	game.run();
}