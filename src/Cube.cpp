#include "Cube.h"


/// <summary>
/// Default constructor
/// </summary>
Cube::Cube() :
	m_position(0.0f, 0.0f, 0.0f),
	m_width(1.0f),
	m_height(1.0f),
	m_depth(1.0f)
{
	initVertices();
}

/// <summary>
/// Destructor
/// </summary>
Cube::~Cube()
{
}

/// <summary>
/// Main cube update logic
/// </summary>
void Cube::update()
{

}

/// <summary>
/// Main cube render logic
/// </summary>
/// <param name="window"> target window of render calls </param>
void Cube::render(sf::Window & window)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(VERTEX_SIZE, GL_FLOAT, 0, &m_vertices);
	glColorPointer(VERTEX_SIZE, GL_FLOAT, 0, &m_colors);

	glDrawElements(GL_TRIANGLES, INDEX_SIZE, GL_UNSIGNED_INT, &m_vertexIndex);

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

void Cube::initVertices()
{
	/**/
	/*		INITIALIAZING VERTICES				*/	
	/**/

	// top right front
	m_vertices[0] = m_position.x + m_width;		// right
	m_vertices[1] = m_position.y;	// top
	m_vertices[2] = m_position.z;	// front

	// top left front
	m_vertices[3] = m_position.x;	// left
	m_vertices[4] = m_position.y;	// top
	m_vertices[5] = m_position.z;	// front
	
	// bottom left front
	m_vertices[6] = m_position.x;	// left
	m_vertices[7] = m_position.y - m_height;	// bottom
	m_vertices[8] = m_position.z;	// front
	
	// bottom right front
	m_vertices[9] = m_position.x + m_width;		// right
	m_vertices[10] = m_position.y - m_height;	// bottom
	m_vertices[11] = m_position.z;	// front

	// top right back
	m_vertices[12] = m_position.x + m_width;	// right
	m_vertices[13] = m_position.y;	// top
	m_vertices[14] = m_position.z - m_depth;	// back
	
	// top left back
	m_vertices[15] = m_position.x;	// left
	m_vertices[16] = m_position.y;	// top
	m_vertices[17] = m_position.z - m_depth;	// back
	
	// bottom left back
	m_vertices[18] = m_position.x;	// left
	m_vertices[19] = m_position.y - m_height;	// bottom
	m_vertices[20] = m_position.z - m_depth;	// back
	
	// bottom right back
	m_vertices[21] = m_position.x + m_width;	// right
	m_vertices[22] = m_position.y - m_height;	// bottom
	m_vertices[23] = m_position.z - m_depth;	// back


	/**/
	/*		INITILIAZING VERTEX INDEXES			*/
	/**/

	// front face
	m_vertexIndex[0] = 0u; m_vertexIndex[1] = 1u; m_vertexIndex[2] = 2u;
	m_vertexIndex[3] = 3u; m_vertexIndex[4] = 0u; m_vertexIndex[5] = 2u;

	// right face
	m_vertexIndex[6] = 0u; m_vertexIndex[7] = 3u; m_vertexIndex[8] = 4u;
	m_vertexIndex[9] = 4u; m_vertexIndex[10] = 0u; m_vertexIndex[11] = 7u;

	// back face
	m_vertexIndex[12] = 4u; m_vertexIndex[13] = 6u; m_vertexIndex[14] = 5u;
	m_vertexIndex[15] = 7u; m_vertexIndex[16] = 6u; m_vertexIndex[17] = 4u;

	// left face
	m_vertexIndex[18] = 5u; m_vertexIndex[19] = 6u; m_vertexIndex[20] = 1u;
	m_vertexIndex[21] = 1u; m_vertexIndex[22] = 6u; m_vertexIndex[23] = 2u;

	// top face
	m_vertexIndex[24] = 5u; m_vertexIndex[25] = 1u; m_vertexIndex[26] = 4u;
	m_vertexIndex[27] = 0u; m_vertexIndex[28] = 4u; m_vertexIndex[29] = 1u;

	// bottom face
	m_vertexIndex[30] = 7u; m_vertexIndex[31] = 2u; m_vertexIndex[32] = 6u;
	m_vertexIndex[33] = 3u; m_vertexIndex[34] = 2u; m_vertexIndex[35] = 7u;

	/**/
	/*		INITIALIAZING COLOURS				*/
	/**/
	const sf::Vector3f GREEN = { 0.0f, 1.0f, 0.0f };
	const sf::Vector3f RED = { 1.0f, 0.0f, 0.0f };
	const sf::Vector3f BLUE = { 0.0f, 0.0f, 1.0f };

	m_colors[0] = GREEN.x; m_colors[1] = GREEN.y; m_colors[2] = GREEN.z;
	m_colors[3] = BLUE.x; m_colors[4] = BLUE.y; m_colors[5] = BLUE.z;
	m_colors[6] = GREEN.x; m_colors[7] = GREEN.y; m_colors[8] = GREEN.z;
	m_colors[9] = RED.x; m_colors[10] = RED.y; m_colors[11] = RED.z;
	m_colors[12] = RED.x; m_colors[13] = RED.y; m_colors[14] = RED.z;
	m_colors[15] = GREEN.x; m_colors[16] = GREEN.y; m_colors[17] = GREEN.z;
	m_colors[18] = BLUE.x; m_colors[19] = BLUE.y; m_colors[20] = BLUE.z;
	m_colors[21] = GREEN.x; m_colors[22] = GREEN.y; m_colors[23] = GREEN.z;
}

void Cube::updateVertices()
{
	// top right front
	m_vertices[0] = m_position.x + m_width;		// right
	m_vertices[1] = m_position.y;	// top
	m_vertices[2] = m_position.z;	// front

	// top left front
	m_vertices[3] = m_position.x;	// left
	m_vertices[4] = m_position.y;	// top
	m_vertices[5] = m_position.z;	// front

	// bottom left front
	m_vertices[6] = m_position.x;	// left
	m_vertices[7] = m_position.y - m_height;	// bottom
	m_vertices[8] = m_position.z;	// front

	// bottom right front
	m_vertices[9] = m_position.x + m_width;		// right
	m_vertices[10] = m_position.y - m_height;	// bottom
	m_vertices[11] = m_position.z;	// front

	// top right back
	m_vertices[12] = m_position.x + m_width;	// right
	m_vertices[13] = m_position.y;	// top
	m_vertices[14] = m_position.z - m_depth;	// back

	// top left back
	m_vertices[15] = m_position.x;	// left
	m_vertices[16] = m_position.y;	// top
	m_vertices[17] = m_position.z - m_depth;	// back

	// bottom left back
	m_vertices[18] = m_position.x;	// left
	m_vertices[19] = m_position.y - m_height;	// bottom
	m_vertices[20] = m_position.z - m_depth;	// back

	// bottom right back
	m_vertices[21] = m_position.x + m_width;	// right
	m_vertices[22] = m_position.y - m_height;	// bottom
	m_vertices[23] = m_position.z - m_depth;	// back
}
