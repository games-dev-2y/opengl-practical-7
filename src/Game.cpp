#include <Game.h>

bool flip = false;
int current = 1;

Game::Game(const sf::ContextSettings & settings) :
	m_window(sf::VideoMode(800, 600), "OpenGL Cube", sf::Style::Default, settings)
{
}

Game::~Game() {}

unsigned int const VERTICES_SIZE = 24u;
unsigned int const VERTEX_SIZE = 3u;

float vertices[VERTICES_SIZE] =
{
	1.0f, 1.0f, 1.0f, // top right front
	-1.0f, 1.0f, 1.0f, // top left front
	-1.0f, -1.0f, 1.0f, // bottom left front
	1.0f, -1.0f, 1.0f, // bottom right front

	1.0f, 1.0f, -1.0f, // top right back
	-1.0f, 1.0f, -1.0f, // top left back
	-1.0f, -1.0f, -1.0f, // bottom left back
	1.0f, -1.0f, -1.0f // bottom right back
};

float colors[VERTICES_SIZE] =
{
	0.0f, 1.0f, 0.0f, // green
	0.0f, 0.0f, 1.0f, // blue
	0.0f, 1.0f, 0.0f, // green
	1.0f, 0.0f, 0.0f, // red
	1.0f, 0.0f, 0.0f, // red
	0.0f, 1.0f, 0.0f, // green
	0.0f, 0.0f, 1.0f, // blue
	0.0f, 1.0f, 0.0f // green
};

unsigned int const INDEX_SIZE = 36u;

unsigned int vertex_index[INDEX_SIZE] =
{
	0u, 1u, 2u, // front face
	3u, 0u, 2u,

	0u, 3u, 4u, // right face
	4u, 3u, 7u,

	4u, 6u, 5u, // back face
	7u, 6u, 4u,

	5u, 6u, 1u, // left face
	1u, 6u, 2u,

	5u, 1u, 4u, // top face
	0u, 4u, 1u,

	7u, 2u, 6u, // bottom face
	3u, 2u, 7u
};

void Game::run()
{

	initialize();


	while (isRunning) {

		std::cout << "Game running..." << std::endl;

		processEvents();
		update();
		render();
	}

}

void Game::initialize()
{
	isRunning = true;

	//glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, m_window.getSize().x / m_window.getSize().y, 1.0, 7.0);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(0.0f, 0.0f, -5.0f);
	glEnable(GL_CULL_FACE);
}

void Game::processEvents()
{
	sf::Event event;

	while (m_window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			isRunning = false;
			break;
		case sf::Event::KeyPressed:
			processKeyHandling(event.key.code);
			break;
		default:
			break;
		}
	}
}

void Game::processKeyHandling(const sf::Keyboard::Key & keyPressed)
{
	const double ANGLE = 2.0;
	const double SCALE_DOWN = 0.9;
	const double SCALE_UP = 1.1;
	const double TRANSLATE = 0.1;
	switch (keyPressed)
	{
	case sf::Keyboard::Escape:
		isRunning = false;
		break;
	case sf::Keyboard::Up:
		applyTransform(Matrix3::rotation(Matrix3::Axis::X, -ANGLE));
		break;
	case sf::Keyboard::Down:
		applyTransform(Matrix3::rotation(Matrix3::Axis::X, ANGLE));
		break;
	case sf::Keyboard::Left:
		applyTransform(Matrix3::rotation(Matrix3::Axis::Z, ANGLE));
		break;
	case sf::Keyboard::Right:
		applyTransform(Matrix3::rotation(Matrix3::Axis::Z, -ANGLE));
		break;
	case sf::Keyboard::Q:
		applyTransform(Matrix3::scale(SCALE_DOWN, SCALE_DOWN, SCALE_DOWN));
		break;
	case sf::Keyboard::E:
		applyTransform(Matrix3::scale(SCALE_UP, SCALE_UP, SCALE_UP));
		break;
	case sf::Keyboard::W:
		applyTransform(Matrix3::translate(0.0, TRANSLATE));
		break;
	case sf::Keyboard::S:
		applyTransform(Matrix3::translate(0.0, -TRANSLATE));
		break;
	case sf::Keyboard::A:
		applyTransform(Matrix3::translate(-TRANSLATE, 0.0));
		break;
	case sf::Keyboard::D:
		applyTransform(Matrix3::translate(TRANSLATE, 0.0));
		break;
	default:
		break;
	}
}

void Game::update()
{
	m_elapsed = m_clock.getElapsedTime();

	std::cout << "Update up" << std::endl;
	
}

void Game::render()
{
	std::cout << "Drawing" << std::endl;
	
	glCullFace(GL_BACK);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // CLEAR THE SCREEN

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glVertexPointer(VERTEX_SIZE, GL_FLOAT, 0, &vertices);
	glColorPointer(VERTEX_SIZE, GL_FLOAT, 0, &colors);
	
	glDrawElements(GL_TRIANGLES, INDEX_SIZE, GL_UNSIGNED_INT, &vertex_index);

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	m_window.display();

}

void Game::unload()
{
	std::cout << "Cleaning up" << std::endl;
}

void Game::applyTransform(const Matrix3 & transform)
{
	for (unsigned i = 0u; i < VERTICES_SIZE; i+= VERTEX_SIZE)
	{
		Vector3 vertex(vertices[i], vertices[i + 1u], vertices[i + 2u]);
		vertex = transform * vertex;
		vertices[i] = static_cast<float>(vertex.getX());
		vertices[i + 1u] = static_cast<float>(vertex.getY());
		vertices[i + 2u] = static_cast<float>(vertex.getZ());
	}
}