# README #

Gameplay Programming Practical 07

### What is this repository for? ###

* Lab 07 Experiment with a Cube and Vertex arrays
* Version 1.0

### How do I get set up? ###

* ensure SFML_SDK environment variable exits
* ensure SFML Version SFML 2.3.2 Visual C++ 14 (2015) - 32-bit is installed
* ([http://www.sfml-dev.org/files/SFML-2.3.2-windows-vc14-32-bit.zip](https://bitbucket.org/tutorials/markdowndemo) "SFML-2.3.2-windows-vc14-32-bit.zip")

### Who do I talk to? ###

* [Rafael Plugge](mailto:rafael.plugge@email.com)