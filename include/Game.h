#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <gl/GL.h>
#include <gl/GLU.h>

#include <Vector3.h>
#include <Matrix3.h>
#include <Quaternion.h>
#include <Cube.h>

class Game
{
public:
	Game(const sf::ContextSettings & settings = sf::ContextSettings());
	~Game();
	void run();
private:
	sf::Window m_window;
	bool isRunning = false;
	void initialize();
	void processEvents();
	void processKeyHandling(const sf::Keyboard::Key &);
	void update();
	void render();
	void unload();

	void applyTransform(const Matrix3 &);

	sf::Clock m_clock;
	sf::Time m_elapsed;

	float m_rotationAngle = 0.0f;

	Matrix3 m_transform;

	Cube m_cube;
};