#ifndef CUBE
#define CUBE

#include "SFML\Graphics.hpp"
#include "SFML\OpenGL.hpp"

class Cube
{
public:
	Cube();
	~Cube();

	void update();
	void render(sf::Window &);

private:
	void initVertices();
	void updateVertices();

	static unsigned int const VERTICES_SIZE = 24u;
	static unsigned int const VERTEX_SIZE = 3u;
	static unsigned int const INDEX_SIZE = 36u;

	sf::Vector3f m_position; // position of the top left front of the cube

	float m_width;
	float m_height;
	float m_depth;
	float m_vertices[VERTICES_SIZE];
	float m_colors[VERTICES_SIZE];
	unsigned int m_vertexIndex[INDEX_SIZE];

};

#endif // !CUBE